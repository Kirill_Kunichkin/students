package info.androidhive.sqlite.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.students.R;

import java.io.File;
import java.util.List;

import info.androidhive.sqlite.database.model.Note;
import info.androidhive.sqlite.utils.ImageHelper;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.MyViewHolder> {

    private Context context;
    private List<Note> notesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView photo;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.firstName);
            photo = view.findViewById(R.id.sm_photo);
        }
    }


    public NotesAdapter(Context context, List<Note> notesList) {
        this.context = context;
        this.notesList = notesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Note note = notesList.get(position);

        holder.name.setText(note.getFirstName() + " " + note.getLastName());
        try {
            setImageFromPath(note.getPhoto(), holder.photo);
        } catch (NullPointerException e){

        }
        // Displaying dot from HTML character code

    }

    private void setImageFromPath(String s, ImageView image) {
        File imgFile = new File(s);
        if (imgFile.exists()) {
            Bitmap myBitmap = ImageHelper.decodeSampleBitmapFromPath(imgFile.getAbsolutePath(), 200, 200);
            image.setImageBitmap(myBitmap);
        }
    }

    @Override
    public int getItemCount() {
        return notesList.size();
    }
}
