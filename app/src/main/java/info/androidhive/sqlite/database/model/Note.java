package info.androidhive.sqlite.database.model;

public class Note {
    public static final String TABLE_NAME = "students";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_FIRST_NAME = "firstName";
    public static final String COLUMN_LAST_NAME = "lastName";
    public static final String COLUMN_PATRONYMIC = "patronymic";
    public static final String COLUMN_GROUP = "class";
    public static final String COLUMN_FACULTY = "faculty";
    public static final String COLUMN_AGE = "age";
    public static final String COLUMN_PHOTO = "photo";
    public static final String COLUMN_MORE = "more";

    private int id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String group;
    private String faculty;
    private int age;
    private String photo;
    private String more;


    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_FIRST_NAME + " TEXT,"
                    + COLUMN_LAST_NAME + " TEXT,"
                    + COLUMN_PATRONYMIC + " TEXT,"
                    + COLUMN_GROUP + " TEXT,"
                    + COLUMN_AGE + " INTEGER,"
                    + COLUMN_FACULTY + " TEXT, "
                    + COLUMN_PHOTO + " TEXT, "
                    + COLUMN_MORE + " TEXT"
                    +")";

    public Note(int id, String firstName, String lastName, String patronymic, String group, String faculty, int age, String photo, String more) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.group = group;
        this.faculty = faculty;
        this.age = age;
        this.photo = photo;
        this.more = more;
    }

    public Note() {
    }
    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }
    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getMore() {
        return more;
    }

    public void setMore(String more) {
        this.more = more;
    }
}