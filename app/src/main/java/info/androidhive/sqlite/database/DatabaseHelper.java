package info.androidhive.sqlite.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.sqlite.database.model.Note;

public class DatabaseHelper extends SQLiteOpenHelper {


    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "students";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create notes table
        db.execSQL(Note.CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + Note.TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    public long insertNote(String firstName, String lastName, String patronymic, String group, String faculty, int age, String photo, String more) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Note.COLUMN_FIRST_NAME, firstName);
        values.put(Note.COLUMN_LAST_NAME, lastName);
        values.put(Note.COLUMN_PATRONYMIC, patronymic);
        values.put(Note.COLUMN_GROUP, group);
        values.put(Note.COLUMN_AGE, age);
        values.put(Note.COLUMN_FACULTY, faculty);
        values.put(Note.COLUMN_PHOTO, photo);
        values.put(Note.COLUMN_MORE, more);
        // insert row
        long id = db.insert(Note.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public Note getNote(long id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Note.TABLE_NAME,
                null, Note.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare note object
        Note note = new Note(
                cursor.getInt(cursor.getColumnIndex(Note.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(Note.COLUMN_FIRST_NAME)),
                cursor.getString(cursor.getColumnIndex(Note.COLUMN_LAST_NAME)),
                cursor.getString(cursor.getColumnIndex(Note.COLUMN_PATRONYMIC)),
                cursor.getString(cursor.getColumnIndex(Note.COLUMN_GROUP)),
                cursor.getString(cursor.getColumnIndex(Note.COLUMN_FACULTY)),
                cursor.getInt(cursor.getColumnIndex(Note.COLUMN_AGE)),
                cursor.getString(cursor.getColumnIndex(Note.COLUMN_PHOTO)),
                cursor.getString(cursor.getColumnIndex(Note.COLUMN_MORE))
        );
        // close the db connection
        cursor.close();

        return note;
    }

    public List<Note> getAllNotes() {
        List<Note> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + Note.TABLE_NAME + " ORDER BY " +
                Note.COLUMN_FACULTY + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Note note = new Note();
                note.setId(cursor.getInt(cursor.getColumnIndex(Note.COLUMN_ID)));
                note.setFirstName(cursor.getString(cursor.getColumnIndex(Note.COLUMN_FIRST_NAME)));
                note.setLastName(cursor.getString(cursor.getColumnIndex(Note.COLUMN_LAST_NAME)));
//                note.setPatronymic(cursor.getString(cursor.getColumnIndex(Note.COLUMN_PATRONYMIC)));
//                note.setGroup(cursor.getString(cursor.getColumnIndex(Note.COLUMN_GROUP)));
//                note.setFaculty(cursor.getString(cursor.getColumnIndex(Note.COLUMN_FACULTY)));
//                note.setAge(cursor.getInt(cursor.getColumnIndex(Note.COLUMN_AGE)));
//                note.setPhoto(cursor.getString(cursor.getColumnIndex(Note.COLUMN_PHOTO)));
//                note.setMore(cursor.getString(cursor.getColumnIndex(Note.COLUMN_MORE)));
                notes.add(note);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }

    public int getNotesCount() {
        String countQuery = "SELECT  * FROM " + Note.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

    public void updateNote(String firstName, String lastName, String patronymic, String group, String faculty, int age, String photo, String more, int position) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Note.COLUMN_FIRST_NAME, firstName);
        values.put(Note.COLUMN_LAST_NAME, lastName);
        values.put(Note.COLUMN_PATRONYMIC, patronymic);
        values.put(Note.COLUMN_GROUP, group);
        values.put(Note.COLUMN_AGE, age);
        values.put(Note.COLUMN_FACULTY, faculty);
        values.put(Note.COLUMN_PHOTO, photo);
        values.put(Note.COLUMN_MORE, more);
        // updating row
        db.update(Note.TABLE_NAME, values, Note.COLUMN_ID + "=?",
                new String[]{Integer.toString(position)});

    }

    public void deleteNote(Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Note.TABLE_NAME, Note.COLUMN_ID + " =?",
                new String[]{String.valueOf(note.getId())});
        db.close();
    }
}